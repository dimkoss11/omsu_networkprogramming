#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <string.h>
#include <time.h> 
#include <ctype.h> 
#include <sys/types.h> 
#include <sys/socket.h> 
#include <arpa/inet.h> 
#include <netinet/in.h>
  
#define PORT     1147

void error(const char *msg);
void add_state_string(char* buffer, int number2);
char* get_random_string(int length);
void split(char* buffer, int sockfd, struct sockaddr_in *cliaddr);
void send_data(char* buf, int sockfd, struct sockaddr_in *cliaddr);
int integer_check(char* s);
void server_return_error(int sockfd, int code, struct sockaddr_in *cliaddr);
void read_command(int sockfd, char* buffer, struct sockaddr_in *cliaddr);

void error(const char *msg)
{
    perror(msg);
    exit(0);
}

char* get_random_string(int length)
{
    char alphabet[10]="abcdefghij";
    char* random_string = malloc(sizeof(char)*length);
    *random_string = '\0';

    srand(time(NULL));

    for (int i = 0; i < length; ++i) {
        char random_char = alphabet[rand() % strlen(alphabet)];
        random_string[i] = random_char;
    }
    return random_string;
}

int integer_check(char* s)
{
    int valid = 0;
    int len = strlen(s);
    while (len > 0 && isspace(s[len - 1]))
        len--;
    if (len > 0)
    {
        valid = 1;
        for (int i = 0; i < len; ++i)
        {
            if (!isdigit(s[i]))
            {
                valid = 0;
                break;
            }
        }
    }
    return valid;
}

void send_data(char* msg, int sockfd, struct sockaddr_in *cliaddr)
{
    sendto(sockfd, msg, strlen(msg), 0, (const struct sockaddr *) &cliaddr, sizeof(struct sockaddr));
}

void split(char* buffer, int sockfd, struct sockaddr_in *cliaddr)
{
    int string_l = strlen(buffer);
    if (string_l <= 940)
    {
        add_state_string(buffer, 0);
        send_data(buffer, sockfd, cliaddr);
    }
    else
    {
        for (int i = 0; i <= (string_l / 940); i++)
        {
            char* buf = malloc(sizeof(char)*1000);
            memset(buf, 0, sizeof(char)*1000);
            if (i != (string_l / 940))
            {
                strncpy(buf, buffer + i * 940, 940);
                printf("%d\n",(int) strlen(buf));
                add_state_string(buf, 1);
                send_data(buf, sockfd, cliaddr);
                free(buf);
            }
            else
            {
                strncpy(buf, buffer + i * 940, 940);
                add_state_string(buf, 0);
                send_data(buf, sockfd, cliaddr);
                free(buf);
            }
        }
    }
}

void add_state_string(char* buffer, int number2)
{
    char state[56] = {'\0'};
    int number1 = 25;
    snprintf(state, 56, "200 OK begin= %d more= %d ", number1, number2);

    char* new_buffer = malloc(strlen(buffer)+57);
    strcpy(new_buffer, state);
    strcat(new_buffer, buffer);
    strcpy(buffer, new_buffer);
}

void server_return_error(int sockfd, int code, struct sockaddr_in *cliaddr)
{
    char* msg = "";
    switch (code) {
        case 400:
            msg = "400 Unknown command\n";
            sendto(sockfd, msg, strlen(msg), 0, (const struct sockaddr *) &cliaddr, 
            sizeof(struct sockaddr));
            printf("%d\n", 400);
            break; 
        case 401:
            msg = "401 Empty argument\n";
            sendto(sockfd, msg, strlen(msg), 0, (const struct sockaddr *) &cliaddr, 
            sizeof(struct sockaddr));
            printf("%d\n", 401);
            break;
        case 402:
            msg =  "402 Argument is not int\n";
            sendto(sockfd, msg, strlen(msg), 0, (const struct sockaddr *) &cliaddr, 
            sizeof(struct sockaddr));
            printf("%d\n", 402);
            break;
        case 500:
            msg = "500 Illegal request format\n";
            sendto(sockfd, msg, strlen(msg), 0, (const struct sockaddr *) &cliaddr, 
            sizeof(struct sockaddr));
            printf("%d\n", 500);
            break;
        case 0:
            msg ="Closing connection\n";
            sendto(sockfd, msg, strlen(msg), 0, (const struct sockaddr *) &cliaddr, 
            sizeof(struct sockaddr));
            printf("%s\n", "quit");
            break;
    }
}

void read_command(int sockfd, char* buffer, struct sockaddr_in *cliaddr) {
    
    bzero(buffer,128);
    int len = sizeof(cliaddr);
    int n = recvfrom(sockfd, buffer, 255, 0 ,( struct sockaddr *) &cliaddr, &len);
    if (n < 0) error("ERROR reading from socket");
    printf("Here is the message: %s\n",buffer);

    if(buffer[0] != '\\')
    {
        server_return_error(sockfd, 500, cliaddr);
        return;
    }

    if(strstr(buffer, "\\quit")!=0)
    {
        server_return_error(sockfd, 0, cliaddr);
        return;
    }

    if(strstr(buffer, "\\get")!=0)
    {
        char buf[50];
        strcpy(buf, buffer);
        strtok(buf, " ");
        char* second = strtok(NULL, " ");
        if (second != NULL)
        {
            if (strlen(second) <= 1)
            {
                server_return_error(sockfd, 401, cliaddr);
                return;
            }
            else
            {
                if (integer_check(second))
                {
                    char* random_string = get_random_string(atoi(second));
                    split(random_string, sockfd, cliaddr);
                    return;
                }
                else
                {
                    server_return_error(sockfd, 402, cliaddr);
                    return;
                }
            }
        }
        else
        {
            server_return_error(sockfd, 401, cliaddr);
            return;
        }
    }

    else
    {
        server_return_error(sockfd, 400, cliaddr);
        return;
    }
}

int main() { 
    int sockfd; 
    char buffer[1024];
    char *hello = "Enter a command \\get number for gen random seq of number length or \\quit \n"; 
    struct sockaddr_in servaddr, cliaddr; 
      
    if ( (sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) { 
        perror("socket creation failed"); 
        exit(EXIT_FAILURE); 
    } 
      
    memset(&servaddr, 0, sizeof(servaddr)); 
    memset(&cliaddr, 0, sizeof(cliaddr)); 
      
    servaddr.sin_family    = AF_INET;
    servaddr.sin_addr.s_addr = INADDR_ANY; 
    servaddr.sin_port = htons(PORT); 
      
    if ( bind(sockfd, (const struct sockaddr *)&servaddr,  
            sizeof(servaddr)) < 0 ) 
    { 
        perror("bind failed"); 
        exit(EXIT_FAILURE); 
    }

    int len = sizeof(cliaddr);
    int n = recvfrom(sockfd, (char *)buffer, 1024,  
                MSG_WAITALL, ( struct sockaddr *) &cliaddr, &len);

    sendto(sockfd, (const char *)hello, strlen(hello),  
MSG_CONFIRM, (const struct sockaddr *) &cliaddr, 
    sizeof(cliaddr));
    while(1)
    {
        read_command(sockfd, buffer, &cliaddr);
    }
    return 0; 
}
#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <string.h> 
#include <ctype.h>
#include <sys/types.h> 
#include <sys/socket.h> 
#include <arpa/inet.h> 
#include <netinet/in.h>
  
#define PORT     1147

void letters_counter(char* buffer);
char* receive(int sockfd, struct sockaddr *cliaddr);
void join(char* buffer, int sockfd, struct sockaddr *cliaddr);
char* read_state_string(char* buffer, int* number2);

void letters_counter(char* buffer)
{
    int counts[10] = { 0 };
    int i;
    size_t len = strlen(buffer);
    for (i = 0; i < len; i++)
    {
        char c = buffer[i];
        if (!isalpha(c)) continue;
            counts[(int)(tolower(c) - 'a')]++;
    }

    for (i = 0; i < 10; i++)
    {
        printf("'%c' has %2d occurrences.\n", i + 'a', counts[i]);
    }
}

char* read_state_string(char* buffer, int* number2)
{
    char* buf = malloc(strlen(buffer) + 1);
    strcpy(buf, buffer);

    strtok(buf, " ");
    strtok(NULL, " ");
    strtok(NULL, " ");
    strtok(NULL, " ");
    strtok(NULL, " ");
    char* t6 = strtok(NULL, " ");
    *number2 = atoi(t6);
    char* t7 = strtok(NULL, " ");
    return t7;
}

char* receive(int sockfd, struct sockaddr *cliaddr)
{
    char* buffer = malloc(sizeof(char)*1024);
    bzero(buffer,sizeof(char)*1024);
    int len = sizeof(cliaddr);
    int n = recvfrom(sockfd, buffer, 1024, 0 ,( struct sockaddr *) &cliaddr, &len);
    return buffer;
}

void join(char* buffer, int sockfd, struct sockaddr *cliaddr)
{
    while(1)
    {
        char* rec_part = receive(sockfd, cliaddr);
        printf("Server : %s\n", rec_part);

        char* buf_copy = malloc(sizeof(char)*1024);
        strcpy(buf_copy, rec_part);
        char* first_token = strtok(buf_copy, " ");

        if (!strcmp(first_token, "200"))
        {  if (buffer == NULL)
            {
                buffer = malloc(sizeof(char)*10240);
                memset(buffer,0,sizeof(char)*10240);
            }
        }
        else
        {
            return;
        }
        int number2;
        char* buf = read_state_string(rec_part, &number2);
        strcat(buffer, buf);

        if (number2 == 1)
        {
            continue;
        }
        else
        {
            letters_counter(buffer);
            return;
        }
    }
}

int main() { 
    int sockfd; 
    char* buffer = NULL;
    char hello[256];
    struct sockaddr_in servaddr;

    if ( (sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) { 
        perror("socket creation failed"); 
        exit(EXIT_FAILURE); 
    } 
  
    memset(&servaddr, 0, sizeof(servaddr)); 
      
    servaddr.sin_family = AF_INET; 
    servaddr.sin_port = htons(PORT); 
    servaddr.sin_addr.s_addr = INADDR_ANY;

    memset(&hello, 0, 255);
    sendto(sockfd, (const char *)hello, strlen(hello),
    MSG_CONFIRM, (const struct sockaddr *) &servaddr,  
        (socklen_t) sizeof(struct sockaddr_in)); 

    while(1)
    {
        join(buffer, sockfd, (struct sockaddr *) &servaddr);

        memset(&hello, 0, 255);
        fgets(hello,255,stdin);
          
        sendto(sockfd, (const char *)hello, strlen(hello), 
            MSG_CONFIRM, (const struct sockaddr *) &servaddr,  
                (socklen_t) sizeof(struct sockaddr_in));
    }
    close(sockfd); 
    return 0; 
}
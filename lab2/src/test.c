#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <string.h> 
#include <time.h> 
#include <ctype.h>

//server part
void add_state_string(char* buffer, int number2);
char* get_random_string(int length);
void split(char* buffer);
void send(char* buf);

//client part
char* read_state_string(char* buffer, int* number2);
char* receive();
void join(char* buffer);
void letters_counter(char* buffer);

char* get_random_string(int length)
{
    char alphabet[10]="abcdefghij";
    char* random_string = malloc(sizeof(char)*length);
    *random_string = '\0';

    srand(time(NULL));

    for (int i = 0; i < length; ++i) {
    	char random_char = alphabet[rand() % strlen(alphabet)];
        random_string[i] = random_char;
    }
    return random_string;
}

int integer_check(char* s)
{
	int valid = 0;
	int len = strlen(s);
	while (len > 0 && isspace(s[len - 1]))
	    len--;     // strip trailing newline or other white space
	if (len > 0)
	{
	    valid = 1;
	    for (int i = 0; i < len; ++i)
	    {
	        if (!isdigit(s[i]))
	        {
	            valid = 0;
	            break;
	        }
	    }
	}
	return valid;
}

void letters_counter(char* buffer)
{
	int counts[10] = { 0 };
	int i;
	size_t len = strlen(buffer);
	for (i = 0; i < len; i++)
	{
	    char c = buffer[i];
	    if (!isalpha(c)) continue;
	    	counts[(int)(tolower(c) - 'a')]++;
	}

	for (i = 0; i < 10; i++)
	{
	    printf("'%c' has %2d occurrences.\n", i + 'a', counts[i]);
	}
}

void add_state_string(char* buffer, int number2)
{
	char state[56] = {'\0'};
	int number1 = 25;
	snprintf(state, 56, "200 OK begin= %d more= %d ", number1, number2);
	// printf("%s\n", state);

	char* new_buffer = malloc(strlen(buffer)+57);
	strcpy(new_buffer, state);
	strcat(new_buffer, buffer);
	strcpy(buffer, new_buffer);
	// printf("%s\n", buffer);
	// printf("%c\n", buffer[number1]);
}

char* read_state_string(char* buffer, int* number2)
{
	char* buf = malloc(strlen(buffer) + 1);
	strcpy(buf, buffer);

	strtok(buf, " ");
	strtok(NULL, " ");
	strtok(NULL, " ");
	strtok(NULL, " ");
	strtok(NULL, " ");
	char* t6 = strtok(NULL, " ");
	*number2 = atoi(t6);
	char* t7 = strtok(NULL, " ");
	return t7;
}

void split(char* buffer)
{
	int string_l = strlen(buffer);
	if (string_l <= 940)
	{
		add_state_string(buffer, 0);
		send(buffer);
	}
	else
	{
		for (int i = 0; i <= (string_l / 940); i++)
		{
			char* buf = malloc(sizeof(char)*1000);
			if (i != (string_l / 940))
			{
				strncpy(buf, buffer + i * 940, 940);
				add_state_string(buf, 1);
				send(buf);
				free(buf);
			}
			else
			{
				strncpy(buf, buffer + i * 940, 940);
				add_state_string(buf, 0);
				send(buf);
				free(buf);
			}
		}
	}
}

void send(char* msg)
{
	// sendto(sockfd, msg, strlen(msg), 0, (const struct sockaddr *) &cliaddr, sizeof(cliaddr));
	printf("%s\n", msg);
}

char* receive()
{
	char* buffer = malloc(sizeof(char)*1024);
	bzero(buffer,1024);
 	//    int len = sizeof(cliaddr);
	// int n = recvfrom(sockfd, buffer, 255, 0 ,( struct sockaddr *) &cliaddr, &len);

	return buffer;
}

void join(char* buffer)
{
	// just for check
	// int c = 0;
	while(1)
	{
		// RECEIVE DATA HERE !!!
		// char* rec_part = receive();
		// join(buffer, rec_part);
	
		// two lines below instead receive()
		// char* random_string = get_random_string(940);
		// if (c == 0)
		// {
		// 	add_state_string(random_string, 1);
		// }
		// else
		// {
		// 	add_state_string(random_string, 0);
		// }
		
		// printf("%s\n", random_string);

		// if error code
		char* random_string = malloc(sizeof(char)*1024);
		strcpy(random_string, "400 Unknown command\n");

		char* first_token = strtok(random_string, " ");
		if (strcmp(first_token, "200"))
        {
        	printf("%s\n%d", "returned", atoi(first_token));
            return;
        }


		int number2;
		char* buf = read_state_string(random_string, &number2);
		strcat(buffer, buf);
		// c++;
		if (number2 == 1)
		{
			continue;
		}
		else
		{
			return;
		}
	}
}

void send_random_string_to_client(int sockfd, int length, int cliaddr)
{
    char* msg = get_random_string(length);
    // sendto(sockfd, msg, strlen(msg), 0, (const struct sockaddr *) &cliaddr, sizeof(cliaddr));
    printf("%s\n", "Random string was sent");

    // server part:
	// char* random_string = get_random_string(3024);
	// split(random_string);
}

void server_return_error(int sockfd, int code, int cliaddr)
{
    char* msg = "";
    switch (code) {
        case 400: 
            msg = "400 Unknown command\n";
            // sendto(sockfd, msg, strlen(msg), 0, (const struct sockaddr *) &cliaddr, 
            // sizeof(cliaddr));
            printf("%d\n", 400);
            break; 
        case 401: 
            msg = "401 Empty argument\n";
            // sendto(sockfd, msg, strlen(msg), 0, (const struct sockaddr *) &cliaddr, 
            // sizeof(cliaddr));
            printf("%d\n", 401);
            break;
        case 402:
            msg =  "402 Argument is not int\n";
            // sendto(sockfd, msg, strlen(msg), 0, (const struct sockaddr *) &cliaddr, 
            // sizeof(cliaddr));
            printf("%d\n", 402);
            break;
        case 500: 
            msg = "500 Illegal request format\n";
            // sendto(sockfd, msg, strlen(msg), 0, (const struct sockaddr *) &cliaddr, 
            // sizeof(cliaddr));
            printf("%d\n", 500);
            break;
        case 0:
            msg ="Closing connection\n";
            // sendto(sockfd, msg, strlen(msg), 0, (const struct sockaddr *) &cliaddr, 
            // sizeof(cliaddr));
            printf("%s\n", "quit");
            break;
    }
}

void read_command(int sockfd, char* buffer, int cliaddr) {
    
    // bzero(buffer,128);
    int len = sizeof(cliaddr);
    // int n = recvfrom(sockfd, buffer, 255, 0 ,( struct sockaddr *) &cliaddr, &len);
    // if (n < 0) error("ERROR reading from socket");
    printf("Here is the message: %s\n",buffer);

    if(buffer[0] != '\\')
    {
        server_return_error(sockfd, 500, cliaddr);
        return;
    }

    if(strstr(buffer, "\\quit")!=0)
    {
        server_return_error(sockfd, 0, cliaddr);
        return;
    }

    if(strstr(buffer, "\\get")!=0)
    {
        char buf[50];
        strcpy(buf, buffer);
        strtok(buf, " ");
        char* second = strtok(NULL, " ");
        if (second != NULL)
        {
            if (strlen(second) <= 1)
            {
                server_return_error(sockfd, 401, cliaddr);
                return;
            }
            else
            {
                if (integer_check(second))
                {
                    send_random_string_to_client(sockfd, atoi(second), cliaddr);
                    return;
                }
                else
                {
                    server_return_error(sockfd, 402, cliaddr);
                    return;
                }
            }
        }
        else
        {
            server_return_error(sockfd, 401, cliaddr);
            return;
        }
    }

    else
    {
        server_return_error(sockfd, 400, cliaddr);
        return;
    }
}




int main()
{

	// server part:
	// char* random_string = get_random_string(3024);
	// split(random_string);

	// client part:

	// char* buffer = malloc(sizeof(char)*1024*10);
	// buffer[0] = '\0';
	// join(buffer);
	// printf("%s\n", buffer);

	read_command(0, "\\get 5\n", 0);
	read_command(0, "\\get 3024\n", 0);
	read_command(0, "\\quit\n", 0);

	read_command(0, "\\fuck rewre\n", 0); //400
	read_command(0, "\\get\n", 0); //401
	read_command(0, "\\get \n", 0); //401
	read_command(0, "\\get tretert\n", 0); //402
	read_command(0, "fweffvdfd\n", 0); //500
	read_command(0, "/fweff\n", 0); //500

	return 0;
}
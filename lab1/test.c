#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

void search_word(char* word_)
{
    int num =0;
    char *string[60];
    char* word = strdup(word_);
    word[strlen(word)-1]='\0';  
    printf("%s\n", word);

    FILE *in_file = fopen("text.txt", "r");
    if (in_file == NULL)
    {
        printf("Error file missing\n");
        exit(-1);
    }

    char stringAnswer[300];
    strcpy(stringAnswer, " ");

    while ( fscanf(in_file,"%s", *string) == 1)
    {
        if(strstr(*string, word)!=0) {
            strcpy(stringAnswer, *string);  
            // strcpy(stringAnswer, "\n");
            num++;
        }
    }
    printf("we found the word %s in the file %d times\n",word,num );
    printf("%s\n", stringAnswer);
    num = 0;
    fclose(in_file);
}

void check_no_arg(char* buffer)
{
    if(strstr(buffer, "\\search")!=0)
    {
        char buf[50];
        strcpy(buf, buffer);
        strtok(buf, " ");
        const char* second = strtok(NULL, " ");
        if (second != NULL)
        {
            if (strlen(second) <= 1)
            {
                printf("%d\n", 401);
            }
            else
            {
                printf("%s\n", second);
            }
            
        }
        else
        {
            printf("%d\n", 401);
        }
    }
}

int main()
{
    char buffer[50];
    strcpy(buffer,"\\search \n");
    check_no_arg(buffer);
    // char stringAnswer[300];
    // strcpy(stringAnswer, " ");
    // strcat(stringAnswer, "First");
    // strcat(stringAnswer, "Second");
    // printf("%s\n", stringAnswer);
    // search_word("string\n");
    return 0;
}
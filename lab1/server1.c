#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

void error(const char *msg)
{
    perror(msg);
    exit(1);
}

int fsize(FILE *fp){
    int prev=ftell(fp);
    fseek(fp, 0L, SEEK_END);
    int sz=ftell(fp);
    fseek(fp,prev,SEEK_SET);
    return sz;
}

void search_word(char* word_, int newsockfd)
{
    int num =0;
    char *string[60];
    char* word = strtok(word_, "\n");
    word = strtok(word, "\r");

    FILE *in_file = fopen("text.txt", "r");
    if (in_file == NULL)
    {
        printf("Error file missing\n");
        exit(-1);
    }

    char stringAnswer[300] = "";

    while ( fscanf(in_file,"%s", *string) == 1)
    {
        if(strstr(*string, word)!=0) {
            strcat(stringAnswer, *string);  
            strcat(stringAnswer, "\n");
            num++;
        }
    }
        fclose(in_file);
    if (num == 0)
    {
        printf("we doesn't found the word %s in the file\n",word);
        char* answ = (char *)malloc(100);
        sprintf(answ, "we doesn't found the word %s in the file\n",word);
        send(newsockfd, answ, strlen(answ), 0);
    }
    else
    {
        printf("we found the word %s in the file %d times\n",word,num );
        printf("%s %d\n", stringAnswer, (int)strlen(stringAnswer));
        char* answer = (char *)malloc(100);
        sprintf(answer, "we found the word %s in the file %d times\n in strings: \n %s",word,num, stringAnswer);
        send(newsockfd, answer, strlen(answer), 0);
    }
    num = 0;
}

void server_return_error(int newsockfd, int code)
{
    switch (code) {
        case 400: 
            send(newsockfd, "400 Unknown command\n", 20, 0);
            break; 
        case 401: 
            send(newsockfd, "401 Empty argument\n", 20, 0);
            break;
        case 402: 
            send(newsockfd, "402 Illegal command\n", 20, 0);
            break;
        case 0:
            send(newsockfd, "Closing connection\n", 20, 0);
            shutdown(newsockfd, SHUT_RDWR);
            close(newsockfd);
            break;
    }
}

void read_command(int newsockfd, char* buffer) {
    
    bzero(buffer,256);
    int n = read(newsockfd,buffer,255);
    if (n < 0) error("ERROR reading from socket");
    printf("Here is the message: %s\n",buffer);

    if(buffer[0] != '\\')
    {
        server_return_error(newsockfd, 402);
        return;
    }

    if(strstr(buffer, "\\quit")!=0)
    {
        server_return_error(newsockfd, 0);
        return;
    }

    if(strstr(buffer, "\\search")!=0)
    {
        if(strtok(buffer, " ") == 0) {
            server_return_error(newsockfd, 401);
        }
        else {
            search_word(strtok(NULL, " "), newsockfd);
        }
    }

    else
    {
        server_return_error(newsockfd, 400);
    }
}

int main(int argc, char *argv[])
{
    int sockfd, newsockfd, portno;
    socklen_t clilen;
    char buffer[256];
    struct sockaddr_in serv_addr, cli_addr;
    int n;
    if (argc < 2) {
        fprintf(stderr,"ERROR, no port provided\n");
        exit(1);
     }

    sockfd =  socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) 
        error("ERROR opening socket");

    bzero((char *) &serv_addr, sizeof(serv_addr));

    portno = atoi(argv[1]);
    serv_addr.sin_family = AF_INET;  
    serv_addr.sin_addr.s_addr = INADDR_ANY;  
    serv_addr.sin_port = htons(portno);

    if (bind(sockfd, (struct sockaddr *) &serv_addr,
              sizeof(serv_addr)) < 0) 
              error("ERROR on binding");
    listen(sockfd,5);

    clilen = sizeof(cli_addr);

    newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
    if (newsockfd < 0) 
        error("ERROR on accept");

     printf("server: got connection from %s port %d\n",
        inet_ntoa(cli_addr.sin_addr), ntohs(cli_addr.sin_port));

    char* hello = "Please enter a command \\search string for search or \\quit for exit: \n";
    send(newsockfd, hello, strlen(hello), 0);

    read_command(newsockfd, buffer);

    bzero(buffer,256);
    n = read(newsockfd,buffer,255);
    if (n < 0) error("ERROR reading from socket");
    printf("Here is the message: %s\n",buffer);

    close(newsockfd);
    close(sockfd);
    return 0; 
}

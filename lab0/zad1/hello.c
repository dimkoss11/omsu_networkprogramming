#include <stdio.h>
#include <getopt.h>
#include <string.h>

int main(int argc, char *argv[]) {

	struct Name
	{
		char* first_name;
		char* last_name;
		char* patronymic;

		int fn;
		int ln;
		int pt;
	} name;

	static struct option long_options[] =
	{
	    {"first", optional_argument, 0, 'f'},
	    {"last", optional_argument, 0, 'l'},
	    {"patronymic", optional_argument, 0, 'p'},
	    {NULL, 0, NULL, 0}
	};

	char res;

	// loop over all of the options
	while ((res = getopt_long(argc, argv, "f:l:p:", long_options, NULL)) != -1)
	{
	    switch (res)
	    {
	         case 'f':
	             name.first_name = optarg;
	             name.fn = 1;
	             break;
	         case 'l':
	             name.last_name = optarg;
	             name.ln = 1;
	             break;
	         case 'p':
	             name.patronymic = optarg;
	             name.pt = 1;
	             break;
	    }
	}
	if (name.fn != 1) {
		printf("%s", "Привет незнакомец!");
	}
	if (name.fn == 1 && name.ln != 1) {
		printf("Привет %s", name.first_name);
	}
	if (name.fn == 1 && name.ln == 1 && name.pt == 1) {
	printf("Привет %s %s %s", name.first_name, name.last_name, name.patronymic);
	}

}
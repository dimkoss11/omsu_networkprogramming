#include <stdio.h>
#include<stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <getopt.h>
#include <string.h>

struct port_address
{
	char* port;
	char* address;
};

struct port_address get_args(int argc , char *argv[])
{
	struct port_address port_addr;

	static struct option long_options[] =
	{
	    {"port", optional_argument, 0, 'p'},
	    {"address", optional_argument, 0, 'a'},
	    {NULL, 0, NULL, 0}
	};

	char res;

	while ((res = getopt_long(argc, argv, "p:a:", long_options, NULL)) != -1)
	{
	    switch (res)
	    {
         case 'p':
             port_addr.port = optarg;
             break;
         case 'a':
             port_addr.address = optarg;
             break;
	    }
	}
	return port_addr;
}

int main(int argc , char *argv[])
{
	struct port_address p_a = get_args(argc, argv);

    int sock;
    struct sockaddr_in server;
    char message[1000] , server_reply[10000];
    
    sock = socket(AF_INET , SOCK_STREAM , 0);
    if (sock == -1)
    {
        printf("Could not create socket");
    }
    puts("Socket created");

    server.sin_addr.s_addr = inet_addr(p_a.address);
    server.sin_family = AF_INET;
    server.sin_port = htons( atoi(p_a.port) );

    if (connect(sock , (struct sockaddr *)&server , sizeof(server)) < 0)
    {
        perror("connect failed. Error");
        return 1;
    }
    
    puts("Connected to the server\n");

	if(recv(sock , server_reply , 2000 , 0) < 0)
    {
        puts("receive failed");
    }
    else {
    	puts(server_reply);
    }
        
    while(1)
    {

        printf("Enter a message : ");
        scanf("%s" , message);
        
        if(send(sock , message , strlen(message) , 0) < 0)
        {
            puts("Send failed");
            return 1;
        }
    }
    
    close(sock);
    return 0;
}